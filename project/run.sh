#!/bin/bash

#mvn install
mvn clean install -f "./project/pom.xml"

#run jar
java -jar "./project/target/test-1.0.jar" "$(pwd)/project/src/main/resources/readings.json" "$(pwd)/project/src/main/resources/rules.json"