package com.hrs.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.hrs.domain.Cond;
import com.hrs.domain.Rule;
import com.hrs.util.FileSystemUtils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class RulesReader {

    private static Logger log = LogManager.getFormatterLogger();

    public static List<Rule> readRulesFile(String path) throws IOException {
        log.info("Reading rules file..");
        List<Rule> result = new ArrayList<Rule>();
        JSONObject sourceRules = new JSONObject(FileSystemUtils.getFileContents(path));
        Iterator<String> types = sourceRules.keys();
        while (types.hasNext()) {
            String type = types.next();
            JSONObject sourceRule = sourceRules.getJSONObject(type);
            Iterator<String> subTypes = sourceRule.keys();
            while (subTypes.hasNext()) {
                String subType = subTypes.next();
                JSONArray subTypeRule = sourceRule.getJSONArray(subType);
                Cond cond = Cond.fromString(subTypeRule.getString(0));
                Integer value = subTypeRule.getInt(1);
                Rule rule = new Rule(cond, type, subType, value);
                result.add(rule);
            }
        }
        return result;
    }
}
