package com.hrs.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.hrs.domain.Patient;
import com.hrs.util.FileSystemUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class JsonPatientFileReader implements BasePatientFileReader {
    Logger log = LogManager.getFormatterLogger();
    @Override
    public List<Patient> readFile(String path) throws IOException {
        log.info("Reading patient file..");
        List<Patient> result = new ArrayList<Patient>();
        String content = FileSystemUtils.getFileContents(path);
        JSONArray patientList = new JSONArray(content);
        for (int i = 0; i < patientList.length(); i++) {
            Patient patient = new Patient();

            JSONObject sourcePatient = patientList.getJSONObject(i);
            patient.setId(sourcePatient.getInt("id"));
            JSONArray sourceReadings = sourcePatient.getJSONArray("readings");
            Map<String, Map<String, Integer>> readings = new HashMap<String, Map<String, Integer>>();
            for (int j = 0; j < sourceReadings.length(); j++) {
                JSONObject readingObject = sourceReadings.getJSONObject(j);
                String typeValue = readingObject.getString("type");
                readingObject.remove("type");
                Iterator<String> keys = readingObject.keys();
                Map<String, Integer> readingMap = new HashMap<String, Integer>();
                while (keys.hasNext()) {
                    String key = keys.next();
                    Integer value = readingObject.getInt(key);
                    readingMap.put(key, value);
                }
                readings.put(typeValue, readingMap);
            }
            patient.setReadings(readings);
            result.add(patient);
        }
        return result;
    }

}
