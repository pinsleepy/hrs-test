package com.hrs.dao;

import java.io.IOException;
import java.util.List;

import com.hrs.domain.Patient;

public interface BasePatientFileReader {
    public List<Patient> readFile(String path) throws IOException;

}
