package com.hrs;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.hrs.dao.BasePatientFileReader;
import com.hrs.dao.JsonPatientFileReader;
import com.hrs.dao.RulesReader;
import com.hrs.domain.Patient;
import com.hrs.domain.PatientRuleViolation;
import com.hrs.domain.Rule;
import com.hrs.service.RulesValidator;
import com.hrs.service.SecondaryServiceClient;
import com.hrs.util.FileSystemUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Test {
    Logger log = LogManager.getFormatterLogger();

    public static void main(String[] args) throws InterruptedException, IOException {
        String readingsFile = args.length >= 1 ? args[0] : null;
        String rulesFiles = args.length >= 2 ? args[1] : "rules.json";
        new Test(rulesFiles, readingsFile);

        Thread.sleep(5);
    }

    public Test(String rulesFile, String readingsFile) throws IOException {
        
        BasePatientFileReader reader = new JsonPatientFileReader();
        SecondaryServiceClient ssc = new SecondaryServiceClient();
        // Read resource source file for values
        List<Patient> patientList = reader.readFile(
            readingsFile);
        // Read resource source file for rules
        List<Rule> rulesList = RulesReader.readRulesFile(
            rulesFile);

        RulesValidator rv = new RulesValidator();
        // See which values trigger the rules
        Map<Integer, List<PatientRuleViolation>> result = rv.validatePatientList(rulesList, patientList);
        // Report the findings to another server
        ssc.postParsedPatientReadings(result);
        // Write findings as json file to /project/results.json
        FileSystemUtils.createFile(result, "project"+File.separator+"results.json");

        
    }
}
