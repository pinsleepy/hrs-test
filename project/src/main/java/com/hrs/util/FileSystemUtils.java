package com.hrs.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class FileSystemUtils {
    private static Logger log = LogManager.getFormatterLogger();
    public static String getFileContents(String path) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
        StringBuilder content = new StringBuilder();
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            content.append(line);
            content.append(System.lineSeparator());
        }
        bufferedReader.close();
        return content.toString();
    }

    public static void createFile(Object inputObj, String path)
            throws JsonGenerationException, JsonMappingException, IOException {

        ObjectMapper om = new ObjectMapper();
        om.writeValue(new File(path), inputObj);
        log.info("Created results.json file in " + System.getProperty("user.dir") + "/project directory");      
    }
}
