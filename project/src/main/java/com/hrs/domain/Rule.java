package com.hrs.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Rule {
    private Cond condition;
    private String type;
    private String subType;
    private Integer value;

    public Rule(Cond condition, String type, String subType, Integer value) {
        this.condition = condition;
        this.type = type;
        this.subType = subType;
        this.value = value;
    }

    public Cond getCondition() {
        return condition;
    }

    public void setCondition(Cond condition) {
        this.condition = condition;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("condition", condition)
                .append("type", type).append("subType", subType).append("value", value).toString();
    }
}
