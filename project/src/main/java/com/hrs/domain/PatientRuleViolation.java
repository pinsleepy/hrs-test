package com.hrs.domain;

public class PatientRuleViolation {
    private Rule rule;
    private Integer patientValue;

    public PatientRuleViolation(Rule rule, Integer patientValue) {
        this.rule = rule;
        this.patientValue = patientValue;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public Integer getPatientValue() {
        return patientValue;
    }

    public void setPatientValue(Integer patientValue) {
        this.patientValue = patientValue;
    }

}
