package com.hrs.domain;

public enum Cond {
    L("<"), G(">"), LE("<="), GE(">=");

    private String value;

    private Cond(String value) {
        this.value = value;
    }

    private String getValue() {
        return this.value;
    }

    public static Cond fromString(String str) {
        for (Cond c : Cond.values()) {
            if (c.getValue().equals(str)) {
                return c;
            }
        }
        return null;
    }
}
