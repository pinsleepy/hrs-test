package com.hrs.domain;

import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Patient {
    private Integer id;
    private Map<String, Map<String, Integer>> readings;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Map<String, Map<String, Integer>> getReadings() {
        return readings;
    }

    public void setReadings(Map<String, Map<String, Integer>> readings) {
        this.readings = readings;
    }

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", id).toString();
    }
}
