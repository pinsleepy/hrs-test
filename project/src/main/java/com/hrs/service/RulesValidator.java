package com.hrs.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.hrs.domain.Cond;
import com.hrs.domain.Patient;
import com.hrs.domain.PatientRuleViolation;
import com.hrs.domain.Rule;

public class RulesValidator {

    public Map<Integer, List<PatientRuleViolation>> validatePatientList(List<Rule> rules, List<Patient> patients)
            throws JsonGenerationException, JsonMappingException, IOException {
        Map<Integer, List<PatientRuleViolation>> parsedResult = new HashMap<Integer, List<PatientRuleViolation>>();
        for (Patient patient : patients) {
            List<PatientRuleViolation> triggeredRules = new ArrayList<>();
            for (Rule rule : rules) {
                if (patient.getReadings().get(rule.getType()) != null
                        && patient.getReadings().get(rule.getType()).get(rule.getSubType()) != null) {
                    if (validate(patient.getReadings().get(rule.getType()).get(rule.getSubType()), rule.getValue(),
                            rule.getCondition())) {
                        PatientRuleViolation prv = new PatientRuleViolation(rule,
                                patient.getReadings().get(rule.getType()).get(rule.getSubType()));
                        triggeredRules.add(prv);
                    }
                }
            }
            parsedResult.put(patient.getId(), triggeredRules);

        }
        return parsedResult;
    }

    public boolean validate(Integer value, Integer ruleValue, Cond cond) {
        switch (cond) {
            case L:
                return value < ruleValue;
            case G:
                return value > ruleValue;
            case LE:
                return value <= ruleValue;
            case GE:
                return value >= ruleValue;
            default:
                return false;
        }

    }
}
