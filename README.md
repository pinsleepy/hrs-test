## Execute
To run the application run the `run.sh` file at the root folder.

## Results
After the code execution a results.json file will be created in project dir which will contain information of patient id, the rule that got triggered/violated and the actual value of the reading